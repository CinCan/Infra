#!/usr/bin/env bash

# letsencrypt wants an email address, fill out in place of CHANGE_THIS below line and uncomment it
# cert_email=CHANGE_THIS

# make the script error out in case of failures
set -euxo pipefail

# apt can't ask interactive questions
export DEBIAN_FRONTEND=noninteractive


# docker
apt-get -yq install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get update
apt-get -yq install docker-ce docker-ce-cli containerd.io git certbot expect wget
sudo wget -O /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/1.24.1/docker-compose-Linux-x86_64
sudo chmod +x /usr/local/bin/docker-compose 
dns_server=$(systemd-resolve --status | sed -n 's/.*DNS Servers: \(.*\)/\1/p')
myip=$(ip -4 -o addr show eth0  | sed -n 's|.*inet \(.*\)/.*|\1|p')
mydns=$(dig +noall +answer -x $myip \
  @$dns_server | sed -n 's/.*PTR[\t ]\(.*\)\.$/\1/p')

echo $mydns > /etc/rdns-hostname

certbot certonly --standalone -n -d $mydns --agree-tos --email $cert_email --redirect # --test-cert # use test-cert for testing & debugging (or CI runs of) setup scripts, you'll hit LT service rate limits otherwise

# system setup done, next part is installing the pilot setup

id ciuser 2>/dev/null || useradd -m ciuser
lekeydir=/etc/letsencrypt/live/$mydns # this will be mounted in the build container when it's run
mkdir -p /opt/cincan/certs
cp $lekeydir/fullchain.pem /opt/cincan/certs/$mydns.crt
cp $lekeydir/privkey.pem /opt/cincan/certs/$mydns.key
# gitlab common name is the same so don't need to repeat for that.

cat > ~ciuser/pilot-vars.sh <<EOT
export CC_TLS_PUBKEY=/opt/cincan/certs/$mydns.crt
export CC_TLS_PRIVKEY=/opt/cincan/certs/$mydns.key
export CC_HOSTNAME=$mydns
export COMMON_NAME_CONCOURSE=$mydns
export COMMON_NAME_GITLAB=$mydns
export ALT_EXTERNAL_URL_CONCOURSE="<unused>"
export ALT_EXTERNAL_URL_GITLAB="<unused>"
export EXTERNAL_URL_CONCOURSE=$mydns
export EXTERNAL_URL_GITLAB=$mydns
export BUILD_PATH=/opt/cincan
EOT
cd /home/ciuser
su - ciuser -c '
  set -eux
  # rm -rf environment
  git clone -q https://gitlab.com/CinCan/environment.git
  cd environment
  # git checkout <branch-name> # if you want to test against a branch of the environment
  < /dev/urandom tr -dc A-Za-z0-9 | head -c14 > password.txt'
cd /home/ciuser/environment
sudo bash -c '. ~ciuser/pilot-vars.sh && . build.sh'

usermod -aG docker ciuser

# update & upgrade ubuntu provided packages
# apt-get -yq update && apt-get -yq upgrade && apt-get -yq autoremove && apt-get -yq clean
cd /opt/cincan/build && docker-compose up -d

echo Finished
