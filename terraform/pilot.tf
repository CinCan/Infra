# Set the variable value in *.tfvars file
# or using -var="hcloud_token=..." CLI option
variable "hcloud_token" {}
variable "ssh_key_id" {}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = "${var.hcloud_token}"
}

output "ip4" {
  value = "${hcloud_server.ci4.ipv4_address}"
}

output "ip6" {
  value = "${hcloud_server.ci4.ipv6_address}"
}

# Create a server
resource "hcloud_server" "ci4" {
  name = "ci4"
  image = "ubuntu-18.04"
  server_type = "cx21"
  location = "hel1"
  user_data = "${file("pilot-init.bash")}"
  ssh_keys = ["${var.ssh_key_id}"]
}

